import * as firebase from 'firebase';

var config = {
    apiKey: "AIzaSyA3p0tGiO6_bNnVnu-2wtO2fcZPpLCZ3RM",
    authDomain: "todolist-f80ef.firebaseapp.com",
    databaseURL: "https://todolist-f80ef.firebaseio.com",
    projectId: "todolist-f80ef",
    storageBucket: "todolist-f80ef.appspot.com",
    messagingSenderId: "618705007737"
  };

const firebaseAPI = firebase.initializeApp(config);

export const firebaseAuth = firebase.auth();
export const firebaseDatabase = firebase.database();

export default firebaseAPI
