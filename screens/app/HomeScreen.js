import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Container, Content, Header, Form, Input, Item, Button, Label, Icon } from 'native-base';

import {firebaseAuth, firebaseAPI} from '../../api/Firebase';

export default class HomeScreen extends React.Component {
  logout() {
    firebaseAuth.signOut().then(() => {
      this.props.navigation.navigate('LoginScreen');
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Form>
        <Text style = {{textAlign: 'center',fontSize:30 , marginTop: 30}}>Bienvenido</Text>
        <Text style = {{textAlign: 'center',fontSize:15}}>Toque el botón para agregar una lista de tareas</Text>
          <Button style = {{ marginTop: 200 , margin: 10 }} iconLeft full rounded success onPress={() => navigate('ToDoScreen')}>
            <Text>Agregar tarea</Text>
          </Button>
          <Button style = {{ margin: 10 }} iconLeft full rounded danger onPress={this.logout.bind(this)}>
            <Text>Cerrar sesión</Text>
          </Button>
        </Form>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
