import React from 'react';
import { StyleSheet, Text, View, ListView } from 'react-native';
import { Container, Content, Header, Form, Input, Item, Button, Label, Icon, List, ListItem } from 'native-base';

import firebaseAPI from '../../api/Firebase';

var data = [];
export default class ToDoScreen extends React.Component {
  constructor(props){
    super(props);

    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2})

    this.state = {
      listViewData: data,
      newTask: ""
    }
  }

  //Esta función estará comprobando las nuevas tareas agregadas para actualizar la lista
  componentDidMount(){
    var task = this

    firebaseAPI.database().ref('/tasks').on('child_added', function(data){
      var newTasks = [...task.state.listViewData]
      newTasks.push(data)
      task.setState({listViewData: newTasks})
    })
  }

  createTask(data){
    var key = firebaseAPI.database().ref('/tasks').push().key
    firebaseAPI.database().ref('/tasks').child(key).set({ taskName: data })
  }

  async deleteTask(secId, rowId, rowMap, data){
    await firebaseAPI.database().ref('tasks/' + data.key).set(null)
    .then(() => {this.props.navigation.navigate('ToDoScreen')})
    rowMap[`${secId}${rowId}`].props.closeRow();
    var newTasks = [...task.state.listViewData]
    rowData.split(rowId,1)
    this.setState({listViewData: newTasks});

  }

  showInformation(){

  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content>
          <Item>
            <Input style = {{ backgroundColor: 'white'}} placeholder = "Nombre de la tarea" onChangeText = {(newTask) => this.setState({newTask})} />
            <Button onPress = {() => this.createTask(this.state.newTask)}>
              <Icon name = "add" />
            </Button>
          </Item>
          <Item>
            <List enableEmptySections
            dataSource = {this.ds.cloneWithRows(this.state.listViewData)}
            renderRow = {data => <ListItem><Text>{data.val().taskName}</Text></ListItem>}
            renderRightHiddenRow = {(data,secId,rowId,rowMap) => <Button full danger onPress = {() => this.deleteTask(secId,rowId,rowMap,data)}><Icon name = "trash" /></Button>}
            rightOpenValue = {-75}/>
          </Item>
        </Content>
        <Content>

        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
