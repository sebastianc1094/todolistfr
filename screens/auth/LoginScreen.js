import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Container, Content, Header, Form, Input, Item, Button, Label, Icon } from 'native-base';

//Importación de la API de Firebase para logearse
import firebaseAPI from '../../api/Firebase';

export default class LoginScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {email:'',
    password:''};
  }

  loginUser = (email, password) => {
    try{
      firebaseAPI.auth().signInWithEmailAndPassword(email,password)
      .then(() => {this.props.navigation.navigate('HomeScreen')})
    }catch(error){
      alert('Datos incorrectos');
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container style = {styles.container}>
        <Form>
          <Item floatingLabel>
            <Label>Correo electrónico</Label>
            <Input autoCorrect = {false} autoCapitalize = "none" onChangeText = {(email) => this.setState({email})}/>
          </Item>
          <Item floatingLabel>
            <Label>Contraseña</Label>
            <Input secureTextEntry = {true} autoCorrect = {false} autoCapitalize = "none" onChangeText = {(password) => this.setState({password})}/>
          </Item>
          <Button style = {{ marginTop: 10 }} iconLeft full rounded bordered primary onPress={() => this.loginUser(this.state.email, this.state.password)}>
            <Icon name = 'person' />
            <Text> Iniciar sesión</Text>
          </Button>
          <Button style = {{ marginTop: 10 }} full rounded bordered success onPress={() => navigate('RegisterScreen')}>
            <Text>Registrarse</Text>
          </Button>
        </Form>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    padding: 30,
  },
});
