import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Container, Content, Header, Form, Input, Item, Button, Label, Icon } from 'native-base';

//Importación de la API de Firebase para hacer el registro
import firebaseAPI from '../../api/Firebase';

export default class RegisterScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {email:'',
    password:''};
  }

  static navigationOptions = {
    title: 'Registrarse',
  };

  newUser = (email, password) => {
    try{
      if(this.state.password.lenght<6){
        alert("Ingrese una contraseña de 6 caracteres o más");
        return;
      }
      firebaseAPI.auth().createUserWithEmailAndPassword(email,password)
      .then(() => {alert('Cuenta creada')})
      .then(() => {this.props.navigation.navigate('LoginScreen')})

    }catch(error){
      console.log(error.toString());
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Form>
          <Item floatingLabel>
            <Label>Correo electrónico</Label>
            <Input autoCorrect = {false} autoCapitalize = "none" onChangeText = {(email) => this.setState({email})} />
          </Item>
          <Item floatingLabel>
            <Label>Contraseña</Label>
            <Input secureTextEntry = {true} autoCorrect = {false} autoCapitalize = "none" onChangeText = {(password) => this.setState({password})} />
          </Item>
          <Button style = {{ marginTop: 10 }} full rounded bordered success onPress={() => this.newUser(this.state.email, this.state.password)}>
            <Text>Registrarse</Text>
          </Button>
        </Form>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30
  },
});
